Tor docker container
====================

Docker container with Tor.


Locations
---------

Source of the image is hosted on Bitbucket at
https://bitbucket.org/beli-sk/docker-tor

If you find any problems, please post an issue at
https://bitbucket.org/beli-sk/docker-tor/issues

The built image is located on Docker Hub at
https://hub.docker.com/r/beli/tor/


Pull or build
-------------

The image is built automatically on Docker hub in repository **beli/tor**
and can be pulled using command

    docker pull beli/tor

or if you'd prefer to build it yourself from the source repository

    git clone https://bitbucket.org/beli-sk/docker-tor.git
    cd docker-tor/
    docker build -t beli/tor .


Usage
-----

The container exposes port 9050, where Tor is listening for socks connections.

If you would like to supply your own ``torrc`` configuration file from the
host, you can mount it as volume over ``/etc/tor/torrc`` inside the container.

To persist your TOR identity or hidden services, mount a volume over /tor.
